/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


let selected = [];

function updateButtonDelete(){
    button = document.getElementById('buttonDelete');
    if(selected.length === 0){
        button.innerHTML = "Eliminar"
        button.disabled = true;
        
    }else{
        button.innerHTML = "Eliminar ("+selected.length+") elementos";
        button.disabled = false;
    }
}

function openDeleteDialog(){
    const deleteDialog = document.getElementById('deleteDialog');
    deleteDialog.showModal();
}

function closeDeleteDialog(){
    const deleteDialog = document.getElementById('deleteDialog');
    deleteDialog.close();
}

function deleteElements(){
    let urldetele = "/practica2/"+model+"/delete?"
    for(let i = 0; i <selected.length; i++){
        urldetele += "id="+selected[i]+"&"
    }
    urldetele = urldetele.slice(0,-1);
    window.location.href = urldetele;
    
}


function load() { 
    checkboxes = document.getElementsByName("selectDrop"); 

    for (let i = 0; i < checkboxes.length; i++) {
        var checkbox = checkboxes[i];
        checkbox.onclick = function() {
            if (this.checked === true){
                selected.push(this.id);
            } else {
                selected = selected.filter(item => item !== this.id)
            }
            updateButtonDelete();
    };
} 
    
} 

document.addEventListener("DOMContentLoaded", load, false);
