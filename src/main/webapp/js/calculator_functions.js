/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global list_car, circuit_selected */

let car_selected = null;
let circuit_selected = null;

//Annadimos callback para actualizar el apartado de informacion de coches
function selectCar(){
    car_selected = list_car[this.value];
    document.getElementById("container-calculator").innerHTML = "";
    document.getElementById("container-car").innerHTML = 
            "<h3>Coche seleccionado: </h3>"+
            "<div><strong>Nombre: </strong>"+car_selected["name"]+"</div>"+
            "<div><strong>Ganancia: </strong>"+car_selected["ganancy"]+"</div>";
}

//Annadimos callback para actualizar el apartado de informacion de coches
function selectCircuit(){
    circuit_selected = list_circuit[this.value];
    document.getElementById("container-calculator").innerHTML = "";
    document.getElementById("container-circuit").innerHTML = 
            "<h3>Cicuito seleccionado: </h3>"+
            "<div><strong>Nombre: </strong>"+circuit_selected["name"]+"</div>"+
            "<div><strong>Pais: </strong>"+circuit_selected["country"]+"</div>"+
            "<div><strong>Ciudad: </strong>"+circuit_selected["city"]+"</div>"+
            "<div><strong>Vueltas: </strong>"+circuit_selected["n_vueltas"]+"</div>"+
            "<div><strong>Longitud: </strong>"+circuit_selected["length"]+"</div>"+
            "<div><strong>Curvas: </strong>"+circuit_selected["n_curvas"]+"</div>";
}

function resetForm(){
    document.getElementsByName("carSelected")[0].value = "";
    document.getElementsByName("circuitSelected")[0].value = "";
    
    document.getElementById("container-car").innerHTML = "";
    document.getElementById("container-circuit").innerHTML = "";
    document.getElementById("container-calculator").innerHTML = "";
    
    car_selected = null;
    circuit_selected = null;
}


//Funcion para hacer el calculo
function calculate(){
    if((car_selected === null) || (circuit_selected === null)){
        alert('Debe especificar un coche y un circuito para calcular la ganancia');
    }else{
        const energy = car_selected["ganancy"]*circuit_selected["n_vueltas"]*circuit_selected["n_curvas"];
        document.getElementById("container-calculator").innerHTML = 
                "<h2>Energía obtenida: </h2>"+
                "<p><strong>"+energy+" julios</strong></p>";
    }
    
}

function load() { 
  const carSelected = document.getElementsByName('carSelected')[0];
  const circuitSelected = document.getElementsByName('circuitSelected')[0];
  
  carSelected.addEventListener("input", selectCar, false);
  circuitSelected.addEventListener("input", selectCircuit, false);
} 

document.addEventListener("DOMContentLoaded", load, false);


