<%-- 
    Document   : CarTableView
    Created on : 30-oct-2020, 8:26:53
    Author     : febis97
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/practica2/css/style.css">
        <link rel="stylesheet" href="/practica2/css/styleTable.css">
        <title>Calculadora Formula 1</title>
        <script>
            var model="car";
        </script>
        <script src="/practica2/js/table_elements.js"></script>
    </head>
    <body>
        <%-- Mostramos el resultado de alguna operacion anterior --%>
        <%   
            String message_result=(String) session.getAttribute("index_message");  
            if((message_result != null) && (message_result != ""))
            {
                out.print("<h3>"+message_result+"</h3>");              
            }
            session.removeAttribute("index_message"); 
        %>  
        <h1 class="title-view">Coches disponibles:</h1>
        <% if((((ArrayList<Object>)request.getAttribute("array_car")).size() == 0)){%>
               <h3>No exiten coches o circuitos en la base de datos</h3>
        <%} else { %>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col col-1"></div>
                <div class="col col-2">Nombre</div>
                <div class="col col-3">Ganancia</div>
            </li>
                <% ArrayList<Object> array_car = (ArrayList<Object>)request.getAttribute("array_car");
                   for(int i = 0;i<array_car.size();i++){
                       HashMap<String,String> aux_car = (HashMap<String,String>) array_car.get(i);
                       out.print("<li class=\"table-row\">");
                       out.print("<div class=\"col col-1\" data-label=\"\"><input type=\"checkbox\" name=\"selectDrop\" id=\""+aux_car.get("id")+"\"></div>");
                       out.print("<div class=\"col col-2\" data-label=\"Nombre\">"+aux_car.get("name")+"</div>");
                       out.print("<div class=\"col col-3\" data-label=\"Ganancia\">"+aux_car.get("ganancy")+"</div>");
                       out.print("</li>");
                }%>
            
        </ul>
        <br><br>
        <button disabled id="buttonDelete" onclick="openDeleteDialog()">Eliminar</button>
         <%} %>
        <br>
        <a href="/practica2/home">Volver al inicio</a>
        <br>
        <a href="/practica2/car/new">Añadir nuevo coche</a>
        
        <dialog id="deleteDialog">
            <p>¿Estás seguro de eliminar los elementos necesarios?</p>

                <button type="button" onclick="deleteElements()">Eliminar</button>
                <button type="button" onclick="closeDeleteDialog()">Cancelar</button>
        </dialog>
    </body>
</html>
