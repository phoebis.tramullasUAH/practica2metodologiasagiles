<%-- 
    Document   : NewCarView
    Created on : 29-oct-2020, 16:07:36
    Author     : febis97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/practica2/css/style.css">
        <link rel="stylesheet" href="/practica2/css/styleForm.css">
        <title>Calculadora Formula 1</title>
    </head>
    <body>
        <h1 class="title-view">Introduce un nuevo coche</h1>
        
        <form action="/practica2/car/new" method="post">
            <label for="name_car">Nombre del coche: </label>
            <input type="text" required id="name_car" name="name_car">
            <br><br>
            <label for="ganancy">Ganancia (kw): (debe estar entre 4kw y 10kw):</label>
            <input type="number" required id="ganancy" name="ganancy" min="4" max="10">
            <br><br>
            <input type="submit" value="Registrar coche">
            <input type="reset">
            
        </form>
        
        <br><br>
        <a href="/practica2/home">Volver al inicio</a>
    </body>
</html>
