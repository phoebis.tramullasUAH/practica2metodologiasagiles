<%-- 
    Document   : NewCircuitView
    Created on : 29-oct-2020, 16:07:54
    Author     : febis97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="/practica2/css/style.css">
        <link rel="stylesheet" href="/practica2/css/styleForm.css">
        <title>Calculadora de F1</title>
    </head>
    <body>
        <h1 class="title-view">Introduce un nuevo circuito</h1>
        
        <form action="/practica2/circuit/new" method="post">
            <label for="name_circuit">Nombre del circuito: </label>
            <input type="text" required id="name_circuit" name="name_circuit">
            <br><br>
            <label for="country_circuit">País del circuito: </label>
            <input type="text" required id="country_circuit" name="country_circuit">
            <br><br>
            <label for="city_circuit">Ciudad del circuito: </label>
            <input type="text" required id="city_circuit" name="city_circuit">
            <br><br>
            <label for="n_vueltas">Nº de vueltas: (debe estar entre 40 y 80):</label>
            <input type="number" required id="n_vueltas" name="n_vueltas" min="40" max="80">
            <br><br>
            <label for="n_curvas">Nº de curvas (debe estar entre 6 y 20):</label>
            <input type="number" required id="n_curvas" name="n_curvas" min="6" max="20">
            <br><br>
            <label for="long">Longitud de la vuelta(m): (debe estar entre 3000 y 9000):</label>
            <input type="number" required id="length" name="length" min="3000" max="9000">
            <br><br>
            <input type="submit" value="Registrar circuito">
            <input type="reset">
            
        </form>
        
        <br><br>
        <a href="/practica2/home">Volver al inicio</a>
    </body>
</html>
