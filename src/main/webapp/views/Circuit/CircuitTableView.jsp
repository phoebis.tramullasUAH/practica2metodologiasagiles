<%-- 
    Document   : CircuitTableView
    Created on : 02-nov-2020, 13:09:50
    Author     : febis97
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora Formula 1</title>
        <link rel="stylesheet" href="/practica2/css/style.css">
        <link rel="stylesheet" href="/practica2/css/styleTable.css">
        <script>
            var model="circuit";
        </script>
        <script src="/practica2/js/table_elements.js"></script>
    </head>
    <body>
        <%-- Mostramos el resultado de alguna operacion anterior --%>
        <%   
            String message_result=(String) session.getAttribute("index_message");  
            if((message_result != null) && (message_result != ""))
            {
                out.print("<h3>"+message_result+"</h3>");              
            }
            session.removeAttribute("index_message"); 
        %>  
        <h1 class="title-view">Circuitos disponibles:</h1>
        <% if((((ArrayList<Object>)request.getAttribute("array_circuit")).size() == 0)){%>
               <h3>No exiten coches o circuitos en la base de datos</h3>
        <%} else { %>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col col-1"></div>
                <div class="col col-2">Nombre</div>
                <div class="col col-3">Pais</div>
                <div class="col col-4">Ciudad</div>
                <div class="col col-5">Nº de vueltas</div>
                <div class="col col-6">Nº de curvas</div>
                <div class="col col-7">Longitud</div>
            </li>
                <% ArrayList<Object> array_circuit = (ArrayList<Object>)request.getAttribute("array_circuit");
                   for(int i = 0;i<array_circuit.size();i++){
                       HashMap<String,String> aux_circuit = (HashMap<String,String>) array_circuit.get(i);
                       out.print("<li class=\"table-row\">");
                       out.print("<div class=\"col col-1\" data-label=\"\"><input type=\"checkbox\" name=\"selectDrop\" id=\""+aux_circuit.get("id")+"\"></div>");
                       out.print("<div class=\"col col-2\" data-label=\"Nombre\">"+aux_circuit.get("name")+"</div>");
                       out.print("<div class=\"col col-3\" data-label=\"Pais\">"+aux_circuit.get("country")+"</div>");
                       out.print("<div class=\"col col-4\" data-label=\"Ciudad\">"+aux_circuit.get("city")+"</div>");
                       out.print("<div class=\"col col-5\" data-label=\"Nº de vueltas\">"+aux_circuit.get("n_vueltas")+"</div>");
                       out.print("<div class=\"col col-6\" data-label=\"Nº de curvas\">"+aux_circuit.get("n_curvas")+"</div>");
                       out.print("<div class=\"col col-7\" data-label=\"Longitud\">"+aux_circuit.get("length")+"</div>");
                       out.print("</li>");
                }%>
        </ul>
            
        <br><br>
        <button disabled id="buttonDelete" onclick="openDeleteDialog()">Eliminar</button>
        <%} %>
        <a href="/practica2/home">Volver al inicio</a>
        <br>
        <a href="/practica2/circuit/new">Añadir nuevo circuito</a>
        
        <dialog id="deleteDialog">
            <p>¿Estás seguro de eliminar los elementos necesarios?</p>

                <button type="button" onclick="deleteElements()">Eliminar</button>
                <button type="button" onclick="closeDeleteDialog()">Cancelar</button>
        </dialog>
    </body>
</html>
