<%-- 
    Document   : CalculatorView
    Created on : 02-nov-2020, 13:30:16
    Author     : febis97
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="helpers.FunctionsHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora Formula 1</title>
        <link rel="stylesheet" href="/practica2/css/style.css">
        <link rel="stylesheet" href="/practica2/css/styleForm.css">
        <script>
            const list_car =<%=FunctionsHelper.hashMapToJSON((ArrayList<Object>)request.getAttribute("array_car"))%>;
            const list_circuit = <%=FunctionsHelper.hashMapToJSON((ArrayList<Object>)request.getAttribute("array_circuit"))%>;
        </script>
        <script src="/practica2/js/calculator_functions.js"></script>
    </head>
    <body>
           <% if((((ArrayList<Object>)request.getAttribute("array_car")).size() == 0) || (((ArrayList<Object>)request.getAttribute("array_circuit")).size() == 0)){%>
               <h3>No exiten coches o circuitos en la base de datos</h3>
            <%} else { %>
                <form action="" method="get">
                <label for="carSelected">Seleccione algún coche: </label>
                <select id="carSelected" name="carSelected">
                    <option selected disabled hidden style='display: none' value=''></option>
                    <% ArrayList<Object> array_car = (ArrayList<Object>)request.getAttribute("array_car");
                       for(int i = 0;i<array_car.size();i++){
                           HashMap<String,String> aux_car = (HashMap<String,String>) array_car.get(i);
                           out.print("<option value=\""+aux_car.get("id")+"\">"+aux_car.get("name")+"</option>");
                    }%>
                </select>

                <label for="list_circuit">Seleccione algún circuito: </label>
                <select id="circuitSelected" name="circuitSelected">
                    <option selected disabled hidden style='display: none' value=''></option>
                    <% ArrayList<Object> array_circuit= (ArrayList<Object>)request.getAttribute("array_circuit");
                       for(int i = 0;i<array_circuit.size();i++){
                           HashMap<String,String> aux_circuit = (HashMap<String,String>) array_circuit.get(i);
                           out.print("<option value=\""+aux_circuit.get("id")+"\">"+aux_circuit.get("name")+"</option>");
                    }%>
                </select>

                <button type="button" onClick='calculate()'>Calcular Energia</button>
                <button type="button" onClick='resetForm()'>Restablecer</button>

                </form>

                <div id="container-car" class="container-info">

                </div>

                <div id="container-circuit" class="container-info">

                </div>

                <div id="container-calculator">

                </div>
            
        <%}%>
        
        <a href="/practica2/home">Volver al inicio</a>
    </body>
</html>
