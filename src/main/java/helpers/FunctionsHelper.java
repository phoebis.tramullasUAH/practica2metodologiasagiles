/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;

/**
 *
 * @author febis97
 */
public class FunctionsHelper {
    
    /** Funcion para transformar un ArrayList HashMap a JSON, annadiendo el campo ID como clave
     * del valor de todos los objetos de la variable
     * @param list
     * @return 
     */
    public static JSONObject hashMapToJSON(ArrayList<Object> list){
        
        JSONObject jsonResult = new JSONObject();
        for(Object data : list) {
            HashMap<String, String> auxData = (HashMap<String, String>) data;
            jsonResult.put(auxData.get("id"), auxData);
        }
        
        return jsonResult;
        
    }
    
}
