/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Modelo
import models.CarModel;

/**
 *
 * @author febis97
 */
public class CarController extends HttpServlet {
    
    /**
     * Atributos
     */
    
    private CarModel carModel;
            
            
            
    @Override
    public void init(){
        
        //Iniciamos el modelo de datos
        this.carModel = new CarModel();
        
    }
    
  
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * 
     * Procesamiento peticiones GET que lleguen al controlador
     */
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       ServletContext sc = this.getServletContext();
       RequestDispatcher rd = null;
       
       switch(request.getRequestURI()){
           //Vista con la informacion de todos los coches en base de datos
           case "/practica2/car":
               this.prepareData(request);
               rd = sc.getRequestDispatcher("/views/Car/CarTableView.jsp");
               break;
           
           //Vista con el formulario para introducir los datos de un nuevo coche
           case "/practica2/car/new":
               rd = sc.getRequestDispatcher("/views/Car/NewCarView.jsp"); 
               break;
               
           //Eliminamos los elementos indicados en la URL y redirigimos a la pagina principal del modelo
           case "/practica2/car/delete":
              this.deleteCars(request);
              response.sendRedirect("/practica2/car");
              break;
           
           //No existe ninguna ruta, volvemos al index
           default: response.sendRedirect("/practica2/index.jsp"); break;
       }
       
       if(rd != null){
           rd.forward(request, response);
       }
    }
    
    
    
   

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       ServletContext sc = this.getServletContext();
       RequestDispatcher rd = null;
       switch(request.getRequestURI()){
           //Insertamos los datos del nuevo coche
           case "/practica2/car/new":
               this.insertNewCar(request);
               response.sendRedirect("/practica2/car");
               break;
           
           //No existe ninguna ruta, volvemos al index
           default: response.sendRedirect("/practica2/home"); break;
       }
       
       if(rd != null){
           rd.forward(request, response);
       }
       
    }
    
    /** Funciones privadas */

    private void prepareData(HttpServletRequest request){
        request.setAttribute("array_car", this.carModel.getCarData());
    }
    
    private void insertNewCar(HttpServletRequest request){
        //Comprobamos que esten todos los datos
        String name = request.getParameter("name_car");
        String ganancy = request.getParameter("ganancy");
        
        if ((name == null) || (ganancy == null)){
            request.getSession().setAttribute("index_message","Llamada formulada incorrectamente.");
            return;
        }
        
        //Creamos el objeto Map con las varibales introducidas
        HashMap <String,String> carData = new HashMap<>();
        carData.put("name", name);
        carData.put("ganancy", ganancy);

        int result = this.carModel.insertNewCar(carData);
        
        String message_result;
        if(result == 0){message_result = "¡Enhorabuena! Se ha introducido corectamente el coche";}
        else {message_result = "Lo sentimos, se ha producido un fallo al introducir el coche";}
               
        request.getSession().setAttribute("index_message",message_result);
        
    }
    
    private void deleteCars(HttpServletRequest request){
        String[] values = request.getParameterValues("id");
        int result = this.carModel.deleteCars(values);
        
        String message_result;
        if(result == 1){message_result = "¡Enhorabuena! Se han eliminado los coches seleccioandos";}
        else {message_result = "Lo sentimos, se ha producido un fallo al eliminar el coche";}
               
        request.getSession().setAttribute("index_message",message_result);
        
    }


}
