/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Modelo
import models.CircuitModel;

/**
 *
 * @author febis97
 */
public class CircuitController extends HttpServlet {

    private CircuitModel circuitModel;
            
            
            
    @Override
    public void init(){
        
        //Iniciamos el modelo de datos
        this.circuitModel = new CircuitModel();
        
    }
    
  
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * 
     * Procesamiento peticiones GET que lleguen al controlador
     */
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       ServletContext sc = this.getServletContext();
       RequestDispatcher rd = null;
       switch(request.getRequestURI()){
           //Vista con la informacion de todos los circuitos en base de datos
           case "/practica2/circuit":
               request.setAttribute("array_circuit", this.circuitModel.getCircuitData());
               rd = sc.getRequestDispatcher("/views/Circuit/CircuitTableView.jsp");
               break;
           
           //Vista con el formulario para introducir los datos de un nuevo circuito
           case "/practica2/circuit/new":
               rd = sc.getRequestDispatcher("/views/Circuit/NewCircuitView.jsp"); 
               break;
               
           
           //Eliminamos los elementos indicados en la URL y redirigimos a la pagina principal del modelo
           case "/practica2/circuit/delete":
              this.deleteCicuits(request);
              response.sendRedirect("/practica2/circuit");
              break;
           
           //No existe ninguna ruta, volvemos al index
           default: 
               response.sendRedirect("/practica2/index.html"); 
               break;
       }
       
       if(rd != null){
           rd.forward(request, response);
       }
       
    }
    
   

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       ServletContext sc = this.getServletContext();
       RequestDispatcher rd = null;
       switch(request.getRequestURI()){
           //Insertamos los datos del nuevo coche
           case "/practica2/circuit/new":
               this.insertNewCircuit(request);
               response.sendRedirect("/practica2/circuit");
               break;
           
           //No existe ninguna ruta, volvemos al index
           default: response.sendRedirect("/practica2/home"); break;
       }
       
       if(rd != null){
           rd.forward(request, response);
       }
       
    }
    
    /** Funciones privadas */
    
        private void insertNewCircuit(HttpServletRequest request){
        //Comprobamos que esten todos los datos
        String name = request.getParameter("name_circuit");
        String country = request.getParameter("country_circuit");
        String city = request.getParameter("city_circuit");
        String length = request.getParameter("length");
        String n_vueltas = request.getParameter("n_vueltas");
        String n_curvas = request.getParameter("n_curvas");
        
        if ((name == null) || (country == null)|| (city == null)|| (length == null)|| (n_vueltas == null)|| (n_curvas == null)){
            request.getSession().setAttribute("index_message","Llamada formulada incorrectamente.");
            return;
        }
        
        //Creamos el objeto Map con las varibales introducidas
        HashMap <String,String> circuitData = new HashMap<>();
        circuitData.put("name", name);
        circuitData.put("country", country);
        circuitData.put("city", city);
        circuitData.put("length", length);
        circuitData.put("n_vueltas", n_vueltas);
        circuitData.put("n_curvas", n_curvas);
        
        
        

        int result = this.circuitModel.insertNewCircuit(circuitData);
        String message_result;
        if(result == 1){message_result = "¡Enhorabuena! Se ha introducido corectamente el circuito";}
        else {message_result = "Lo sentimos, se ha producido un fallo al introducir el circuito";}
               
        request.getSession().setAttribute("index_message",message_result);
        
                      
        
    }
    
    private void deleteCicuits(HttpServletRequest request){
        String[] values = request.getParameterValues("id");
        int result = this.circuitModel.deleteCircuits(values);
        
        String message_result;
        if(result == 1){message_result = "¡Enhorabuena! Se han eliminado los circuitos seleccioandos";}
        else {message_result = "Lo sentimos, se ha producido un fallo al eliminar los circuitos";}
               
        request.getSession().setAttribute("index_message",message_result);
    }
    

}
