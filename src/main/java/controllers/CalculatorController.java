/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;


import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Modelos
import models.CircuitModel;
import models.CarModel;

/**
 *
 * @author febis97
 */
public class CalculatorController extends HttpServlet {

    /**
     * Atributos
     */
    
    private CarModel carModel;
    private CircuitModel circuitModel;
            
               
    @Override
    public void init(){
        
        //Iniciamos los modelos de datos
        this.carModel = new CarModel();
        this.circuitModel = new CircuitModel();
        
    }
    
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       ServletContext sc = this.getServletContext();
       RequestDispatcher rd = null;
       
       switch(request.getRequestURI()){
           //Vista con la seleccion de coches y circuitos para calcular la energia
           case "/practica2/calculator":
               this.prepareData(request);
               rd = sc.getRequestDispatcher("/views/Calculator/CalculatorView.jsp");
               break;
           
           //No existe ninguna ruta, volvemos al index
           default: response.sendRedirect("/practica2/index.html"); break;
       }
       
       if(rd != null){
           rd.forward(request, response);
       }
    }
    
    private void prepareData(HttpServletRequest request){
        ArrayList<Object> list_car = this.carModel.getCarData();
        ArrayList<Object> list_circuit = this.circuitModel.getCircuitData();
        request.setAttribute("array_car", list_car);
        request.setAttribute("array_circuit", list_circuit);
    }

}
