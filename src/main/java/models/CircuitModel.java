/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author febis97
 */
public class CircuitModel {
    //Atributos
    private Connection con;
    private java.sql.Statement set;
    private ResultSet rs;
    //Constructor
    public CircuitModel() {
        String sURL="jdbc:derby://localhost:1527/sample";
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            this.con = DriverManager.getConnection(sURL,"app","app");
            System.out.println("Se ha conectado");
        }
        catch(Exception e){
            System.out.println("No se ha conectado");
        }
    }
    
    
    
    
    //Metodos
    public int insertNewCircuit(HashMap<String, String> circuitData){
        try{
            int result = -1;
            this.set = this.con.createStatement();
            
            String sqlQuery = "INSERT INTO CIRCUITO (NOMBRE, PAIS, CIUDAD, N_VUELTAS, LONGITUD, N_CURVAS) values ('";
                sqlQuery += circuitData.get("name")+"', '";
                sqlQuery += circuitData.get("country")+"', '";
                sqlQuery += circuitData.get("city")+"',";
                sqlQuery += circuitData.get("n_vueltas")+",";
                sqlQuery += circuitData.get("length")+",";
                sqlQuery += circuitData.get("n_curvas")+")";
                
            int resultado_db = this.set.executeUpdate(sqlQuery);
            if(resultado_db == 1){
                //Informamos que la operacion ha salido correctamente
                result = 0;
            }else{
                result = -1;
            }
            
            set.close();
            return result;
            
        }catch(SQLException e){
            return -1;
        }
    }
    
    public int deleteCircuits(String[] ids){
        try{
            this.set = this.con.createStatement();
            
            String sqlQuery = "DELETE FROM CIRCUITO WHERE id in (";
            for(int i = 0; i<ids.length; i++){
                sqlQuery += ids[i]+",";
            }
            sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 1);
            sqlQuery += ")";
                
            int resultado_db = this.set.executeUpdate(sqlQuery);
            set.close();
            return resultado_db;
            
        }catch(SQLException e){
            return -1;
        }
    }
    
    
    public ArrayList<Object> getCircuitData(){
        try{
            ArrayList<Object> array_car = new ArrayList();
            
            this.set = this.con.createStatement();
            String sqlQuery = "SELECT * FROM CIRCUITO";
            ResultSet resultado_db = this.set.executeQuery(sqlQuery);
            //Generamos la lista con la informacion de todos los coches
            
            HashMap<String,String> aux = new HashMap();
            while(resultado_db.next()){
                aux.put("name", resultado_db.getString("nombre"));
                aux.put("country", resultado_db.getString("pais"));
                aux.put("city", resultado_db.getString("ciudad"));
                aux.put("n_vueltas",(String) resultado_db.getString("n_vueltas"));
                aux.put("length",(String) resultado_db.getString("longitud"));
                aux.put("n_curvas",(String) resultado_db.getString("n_curvas"));
                aux.put("id", resultado_db.getString("id"));
                
                array_car.add(aux);
                aux = new HashMap();
            }
            
            set.close();
            return array_car;
            
        }catch(SQLException e){
            return null;
        }
    }
    
}
