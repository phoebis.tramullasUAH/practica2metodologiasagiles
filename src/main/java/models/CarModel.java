/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author febis97
 */
public class CarModel {
    
    //Atributos
    private Connection con;
    private Statement set;
    private ResultSet rs;
    //Constructor
    public CarModel() {
        String sURL="jdbc:derby://localhost:1527/sample";
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            this.con = DriverManager.getConnection(sURL,"app","app");
            System.out.println("Se ha conectado");
        }
        catch(ClassNotFoundException | SQLException e){
            System.out.println("No se ha conectado");
        }
    }
    
    
    
    
    //Metodos
    public int insertNewCar(HashMap<String, String> carData){
        try{
            int result = -1;
            this.set = this.con.createStatement();
            
            String sqlQuery = "INSERT INTO COCHE (NOMBRE, GANANCIA) values('";
                sqlQuery += carData.get("name")+"', ";
                sqlQuery += carData.get("ganancy")+")";
                
            int resultado_db = this.set.executeUpdate(sqlQuery);
            if(resultado_db == 1){
                //Informamos que la operacion ha salido correctamente
                result = 0;
            }else{
                result = -1;
            }
            
            set.close();
            return result;
            
        }catch(Exception e){
            return -1;
        }
    }
    
    public int deleteCars(String[] ids){
        try{
            this.set = this.con.createStatement();
            
            String sqlQuery = "DELETE FROM COCHE WHERE id in (";
            for(int i = 0; i<ids.length; i++){
                sqlQuery += ids[i]+",";
            }
            sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 1);
            sqlQuery += ")";
                
            int resultado_db = this.set.executeUpdate(sqlQuery);
            set.close();
            return resultado_db;
            
        }catch(SQLException e){
            return -1;
        }
    }
    
    
    public ArrayList<Object> getCarData(){
        try{
            ArrayList<Object> array_car = new ArrayList();
            
            this.set = this.con.createStatement();
            String sqlQuery = "SELECT * FROM COCHE";
            ResultSet resultado_db = this.set.executeQuery(sqlQuery);
            //Generamos la lista con la informacion de todos los coches
            
            HashMap<String,String> aux = new HashMap();
            while(resultado_db.next()){
                aux.put("name", resultado_db.getString("nombre"));
                aux.put("ganancy",(String) resultado_db.getString("ganancia"));
                aux.put("id", resultado_db.getString("id"));
                
                array_car.add(aux);
                aux = new HashMap();
            }
            
            set.close();
            return array_car;
            
        }catch(SQLException e){
            return null;
        }
    }
    
   
    
    
}
